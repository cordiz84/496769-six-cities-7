import {reviewsData} from './reviews-data';
import {ActionType} from '../action';
import {reviews} from '../action-test-mocks';

describe('Reducer: reviewsData', () => {
  it('without additional parameters should return initial state', () => {
    const initialState = {
      reviews: [],
    };
    expect(reviewsData(undefined, {}))
      .toEqual(initialState);
  });

  it('should load reviews', () => {
    const state = {reviews: []};
    const loadReviewsAction = {
      type: ActionType.LOAD_REVIEWS,
      payload: reviews,
    };
    expect(reviewsData(state, loadReviewsAction))
      .toEqual({reviews: reviews});
  });
});
