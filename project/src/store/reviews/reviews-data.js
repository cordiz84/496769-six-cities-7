import {loadReviews} from '../action';
import {createReducer} from '@reduxjs/toolkit';

const initialState = {
  reviews: [],
};

const reviewsData = createReducer(initialState, (builder) => {
  builder
    .addCase(loadReviews, (state, action) => {
      state.reviews = [];
      state.reviews = action.payload;
    });
});

export {reviewsData};
