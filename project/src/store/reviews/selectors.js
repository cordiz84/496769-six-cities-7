import {NameSpace} from '../root-reducer';
import {createSelector} from 'reselect';

export const getReviews = (state) => state[NameSpace.REVIEWS_DATA].reviews;

export const getReviewsSort = createSelector(
  getReviews,
  (reviews) =>  [...reviews].sort((a, b) => b.id - a.id),
);
