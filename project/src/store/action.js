import {createAction} from '@reduxjs/toolkit';

export const ActionType = {
  CHANGE_CITY: 'CHANGE_CITY',
  LOAD_OFFERS: 'LOAD_OFFERS',
  LOAD_OFFERS_BY_ID: 'LOAD_OFFERS_BY_ID',
  LOAD_NEAR: 'LOAD_NEAR',
  LOAD_REVIEWS: 'LOAD_REVIEWS',
  LOAD_FAVORITES: 'LOAD_FAVORITES',
  SORT_OPTION: 'SORT_OPTION',
  REQUIRED_AUTHORIZATION: 'REQUIRED_AUTHORIZATION',
  LOGOUT: 'LOGOUT',
  REDIRECT_TO_ROUTE: 'REDIRECT_TO_ROUTE',
  USER: 'USER',
  OFFER_IS_FAVORITE: 'OFFER_IS_FAVORITE',
  ERROR: 'ERROR',
};

export const offerIsFavorite = createAction(ActionType.OFFER_IS_FAVORITE, (offer) => ({
  payload: offer,
}));

export const changeCity = createAction(ActionType.CHANGE_CITY, (city) =>({
  payload: city,
}));

export const loadOffers = createAction(ActionType.LOAD_OFFERS, (offers) => ({
  payload: offers,
}));

export const loadOffersById = createAction(ActionType.LOAD_OFFERS_BY_ID, (offer) => ({
  payload: offer,
}));


export const loadNear = createAction(ActionType.LOAD_NEAR, (offers) => ({
  payload: offers,
}));

export const loadReviews = createAction(ActionType.LOAD_REVIEWS, (reviews) => ({
  payload: reviews,
}));

export const loadFavorites = createAction(ActionType.LOAD_FAVORITES, (favorites) => ({
  payload: favorites,
}));

export const changeSortOption = createAction(ActionType.SORT_OPTION, (offers) => ({
  payload: offers,
}));

export const requireAuthorization = createAction( ActionType.REQUIRED_AUTHORIZATION, (status) => ({
  payload: status,
}));

export const logout = createAction(ActionType.LOGOUT);

export const redirectToRoute = createAction(ActionType.REDIRECT_TO_ROUTE, (url) => ({
  payload: url,
}));

export const setUser = createAction(ActionType.USER, (user) => ({
  payload: user,
}));

export const setError = createAction(ActionType.ERROR, (error) => ({
  payload: error,
}));

