import {
  loadFavorites,
  loadNear,
  loadOffers, loadOffersById,
  loadReviews,
  logout, offerIsFavorite,
  redirectToRoute,
  requireAuthorization, setError,
  setUser
} from './action';
import {AuthorizationStatus, APIRoute, AppRoute} from '../const';
import {convertToCamel} from '../helpers/helpers';

export const fetchOffersList = () => (dispatch, _getState, api) => (
  api.get(APIRoute.OFFERS)
    .then(({data}) => {
      dispatch(loadOffers(convertToCamel(data)));
    })
    .catch((error) => {
      dispatch(setError(error.message));
      dispatch(loadOffers([]));
    })
);

export const fetchOffersById = (id) => (dispatch, _getState, api) => (
  api.get(`${APIRoute.OFFERS}/${id}`)
    .then(({data}) => {
      dispatch(loadOffersById(convertToCamel(data)));
    })
    .catch((error) => {
      dispatch(setError(error.message));
      dispatch(redirectToRoute(APIRoute.FILED_LOAD_DATA));
    })
);

export const fetchOffersNearbyList = (hotelId) => (dispatch, _getState, api) => (
  api.get(`${APIRoute.OFFERS}/${hotelId}/nearby`)
    .then(({data}) => {
      dispatch(loadNear(convertToCamel(data)));
    }).catch((error) => {
      dispatch(setError(error.message));
      dispatch(loadNear([]));
    })
);

export const checkAuth = () => (dispatch, _getState, api) => (
  api.get(APIRoute.LOGIN)
    .then(({data}) => {
      dispatch(requireAuthorization(AuthorizationStatus.AUTH));
      dispatch(setUser(convertToCamel(data)));
    })
    .catch((error) => {
      dispatch(setError(error.message));
      dispatch(requireAuthorization(AuthorizationStatus.AUTH));
    })
);

export const login = ({email, password}) => (dispatch, _getState, api) => (
  api.post(APIRoute.LOGIN, {email, password})
    .then(({data}) => {
      localStorage.setItem('token', data.token);
      dispatch(setUser(convertToCamel(data)));
      dispatch(requireAuthorization(AuthorizationStatus.AUTH));
      dispatch(redirectToRoute(AppRoute.MAIN));
    })
);

export const signOut = () => (dispatch, _getState, api) => (
  api.delete(APIRoute.LOGOUT)
    .then(() => {
      localStorage.removeItem('token');
      dispatch(logout());
    })
);

export const fetchReviewList = (hotelId) => (dispatch, _getState, api) => (
  api.get(`${APIRoute.REVIEWS}/${hotelId}`)
    .then(({data}) => {
      dispatch(loadReviews(convertToCamel(data)));
    })
    .catch((error) => {
      dispatch(setError(error.message));
      dispatch(loadReviews([]));
    })
);

export const createReview = ({hotelId, review, rating}) => (dispatch, _getState, api) => (
  api.post(`${APIRoute.REVIEWS}/${hotelId}`, {comment: review, rating})
    .then(({data}) => {
      dispatch(setError(null));
      dispatch(loadReviews(convertToCamel(data)));
    }).catch(() => {
      dispatch(setError('error create review'));
    })
);

export const fetchFavoritesList = () => (dispatch, _getState, api) => (
  api.get(APIRoute.FAVORITES)
    .then(({data}) => {
      dispatch(loadFavorites(convertToCamel(data)));
    }).catch(() => {
      dispatch(loadFavorites([]));
    })
);

export const addIsFavorites = (hotelId, status) => (dispatch, _getState, api) => (
  api.post(`${APIRoute.FAVORITES}/${hotelId}/${status | 0}`, '', {
    headers: {
      'x-token': localStorage.getItem('token'),
    },
  })
    .then(({data}) => {
      dispatch(offerIsFavorite(convertToCamel(data)));
    }).catch((error) => {
      dispatch(setError(error.message));
    })
);
