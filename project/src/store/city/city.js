import {changeCity} from '../action';
import {Cities} from '../../const';
import {createReducer} from '@reduxjs/toolkit';

const initialState = {
  city: Cities.PARIS.name,
};

const city = createReducer(initialState, (builder) => {
  builder
    .addCase(changeCity, (state, action) => {
      state.city = action.payload;
    });
});


export {city};
