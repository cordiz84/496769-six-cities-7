import {city} from './city';
import {changeCity} from '../action';
import {Cities} from '../../const';

describe('Reducer: changeCity', () => {
  it('without additional parameters should return initial state', () => {
    const initialState = {
      city: Cities.PARIS.name,
    };
    expect(city(undefined, {}))
      .toEqual(initialState);
  });

  it('should change city', () => {
    const state = {
      city: Cities.PARIS.name,
    };
    const exceptResult = {
      city: Cities.AMSTERDAM.name,
    };
    expect(city(state, changeCity('Amsterdam')))
      .toEqual(exceptResult);
  });
});
