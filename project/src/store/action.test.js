import {
  loadOffers, loadOffersById, loadFavorites, loadNear, loadReviews, changeCity, changeSortOption, setUser,
  logout, requireAuthorization, ActionType, redirectToRoute, offerIsFavorite, setError
} from './action';
import {offers, offer, reviews, favorites, nearby} from './action-test-mocks';
import {AuthorizationStatus} from '../const';

describe('Actions', () => {
  it('Action creator for load offers correct action', () => {
    const expectedAction = {
      type: ActionType.LOAD_OFFERS,
      payload: offers,
    };

    expect(loadOffers(offers)).toEqual(expectedAction);
  });

  it('Action creator for offer is favorite correct action', () => {
    const expectedAction = {
      type: ActionType.OFFER_IS_FAVORITE,
      payload: offer,
    };

    expect(offerIsFavorite(offer)).toEqual(expectedAction);
  });

  it('Action creator for load offer by id correct action', () => {
    const expectedAction = {
      type: ActionType.LOAD_OFFERS_BY_ID,
      payload: offer,
    };

    expect(loadOffersById(offer)).toEqual(expectedAction);
  });

  it('Action creator for load favorites correct action', () => {
    const expectedAction = {
      type: ActionType.LOAD_FAVORITES,
      payload: favorites,
    };

    expect(loadFavorites(favorites)).toEqual(expectedAction);
  });

  it('Action creator for load nearby correct action', () => {
    const expectedAction = {
      type: ActionType.LOAD_NEAR,
      payload: nearby,
    };

    expect(loadNear(nearby)).toEqual(expectedAction);
  });

  it('Action creator for load reviews correct action', () => {
    const expectedAction = {
      type: ActionType.LOAD_REVIEWS,
      payload: reviews,
    };

    expect(loadReviews(reviews)).toEqual(expectedAction);
  });

  it('Action creator for change city correct action', () => {
    const city = 'Paris';
    const expectedAction = {
      type: ActionType.CHANGE_CITY,
      payload: city,
    };

    expect(changeCity(city)).toEqual(expectedAction);
  });

  it('Action creator for change sort option correct action', () => {
    const option = 'Popular';
    const expectedAction = {
      type: ActionType.SORT_OPTION,
      payload: option,
    };

    expect(changeSortOption(option)).toEqual(expectedAction);
  });

  it('Action creator for set user correct action', () => {
    const user = {
      email: 'test@test.ru',
      password: 123456,
    };
    const expectedAction = {
      type: ActionType.USER,
      payload: user,
    };

    expect(setUser(user)).toEqual(expectedAction);
  });

  it('Action creator for logout correct action', () => {

    const expectedAction = {
      type: ActionType.LOGOUT,
    };

    expect(logout()).toEqual(expectedAction);
  });

  it('Action creator for requireAuthorization correct action', () => {
    const status = AuthorizationStatus.AUTH;
    const expectedAction = {
      type: ActionType.REQUIRED_AUTHORIZATION,
      payload: status,
    };

    expect(requireAuthorization(status)).toEqual(expectedAction);
  });

  it('Action creator for redirectToRoute action', () => {
    const url = '/login';
    const expectedAction = {
      type: ActionType.REDIRECT_TO_ROUTE,
      payload: url,
    };

    expect(redirectToRoute(url)).toEqual(expectedAction);
  });

  it('Action creator for setError action', () => {
    const error = 'error';
    const expectedAction = {
      type: ActionType.ERROR,
      payload: error,
    };

    expect(setError(error)).toEqual(expectedAction);
  });
});
