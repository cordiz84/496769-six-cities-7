import {createReducer} from '@reduxjs/toolkit';
import {changeSortOption} from '../action';
import {SortOptions} from '../../const';

const initialState = {
  sortOption: SortOptions.POPULAR,
};

const sortOptions = createReducer(initialState, (builder) => {
  builder
    .addCase(changeSortOption, (state, action) => {
      state.sortOption = action.payload;
    });
});

export {sortOptions};
