import {NameSpace} from '../root-reducer';

export const getSortOption = (state) => state[NameSpace.SORT_OPTIONS].sortOption;
