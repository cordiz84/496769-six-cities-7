import {sortOptions} from './sort-options';
import { SortOptions} from '../../const';
import {changeSortOption} from '../action';

describe('Reducer: sortOptions', () => {
  it('without additional parameters should return initial state', () => {
    const initialState = {
      sortOption: SortOptions.POPULAR,
    };
    expect(sortOptions(undefined, {}))
      .toEqual(initialState);
  });

  it('should change sort option', () => {
    const state = {
      sortOption: SortOptions.POPULAR,
    };
    const exceptResult = {
      sortOption: SortOptions.HIGH_TO_LOW,
    };
    expect(sortOptions(state, changeSortOption('highToLow')))
      .toEqual(exceptResult);
  });
});
