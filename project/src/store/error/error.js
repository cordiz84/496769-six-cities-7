import {setError} from '../action';
import {createReducer} from '@reduxjs/toolkit';

const initialState = {
  error: null,
};

const errorData = createReducer(initialState, (builder) => {
  builder
    .addCase(setError, (state, action) => {
      state.error = null;
      state.error = action.payload;
    });
});

export {errorData};
