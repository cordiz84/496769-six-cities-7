import {setError} from '../action';
import {errorData} from './error';

describe('Reducer: changeCity', () => {
  it('without additional parameters should return initial state', () => {
    const initialState = {
      error: null,
    };
    expect(errorData(undefined, {}))
      .toEqual(initialState);
  });

  it('should set error', () => {
    const state = {
      error: null,
    };
    const exceptResult = {
      error: 'error',
    };
    expect(errorData(state, setError('error')))
      .toEqual(exceptResult);
  });
});
