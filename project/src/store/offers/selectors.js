import {NameSpace} from '../root-reducer';
import {toSortOffers} from '../../helpers/helpers';
import {createSelector} from 'reselect';

export const getOffers = (state) => state[NameSpace.OFFERS_DATA].offers;
export const getOfferById = (state) => state[NameSpace.OFFERS_DATA].offer;
export const getCity = (state) => state[NameSpace.OFFERS_DATA].city;
export const getSortOffers = (state) => state[NameSpace.OFFERS_DATA].sortOption;
export const getIsDataLoaded = (state) => state[NameSpace.OFFERS_DATA].isDataLoaded;
export const getIsDataLoadedById = (state) => state[NameSpace.OFFERS_DATA].isDataLoadedById;
export const getOffersNear= (state) => state[NameSpace.OFFERS_NEAR_DATA].offersNear;
export const getFavorites = (state) => state[NameSpace.OFFERS_DATA].favorites;
export const getIsFavoritesLoad = (state) => state[NameSpace.OFFERS_DATA].isFavoritesLoad;

export const getCitySortOffers = createSelector(
  getOffers,
  getCity,
  getSortOffers,
  (offers, city, sort) => toSortOffers(offers, city, sort),
);

export const getOffersNearMap = createSelector(
  getOffersNear,
  getOfferById,
  (nearby, offer) => [...nearby, offer],
);
