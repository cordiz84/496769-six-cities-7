import {offersData} from './offers-data';
import {ActionType} from '../action';
import {offers, offer, nearby, favorites} from '../action-test-mocks';
import {Cities, SortOptions} from '../../const';
import {offersNearData} from './offers-near-data';
import {convertToCamel} from '../../helpers/helpers';

describe('Reducer: offersData', () => {
  it('without additional parameters should return initial state', () => {
    const initialState = {
      offers: [],
      offer: null,
      favorites: [],
      isDataLoaded: false,
      isDataLoadedById: false,
      isFavoritesLoad: false,
      city: Cities.PARIS.name,
      sortOption: SortOptions.POPULAR,
    };
    expect(offersData(undefined, {}))
      .toEqual(initialState);
  });

  it('should load offers', () => {
    const state = {
      offers: [],
      isDataLoaded: false,
      city: Cities.PARIS.name,
      sortOption: SortOptions.POPULAR,
    };
    const loadOffersAction = {
      type: ActionType.LOAD_OFFERS,
      payload: offers,
    };
    expect(offersData(state, loadOffersAction))
      .toEqual({offers,  isDataLoaded: true,
        city: Cities.PARIS.name,
        sortOption: SortOptions.POPULAR});
  });

  it('should load offer by id', () => {
    const state = {
      isDataLoadedById: false,
      offer: null,
    };
    const loadOffersAction = {
      type: ActionType.LOAD_OFFERS_BY_ID,
      payload: offer,
    };
    expect(offersData(state, loadOffersAction))
      .toEqual({offer,  isDataLoadedById: true});
  });

  it('should load favorites', () => {
    const state = {favorites: [], isFavoritesLoad: false};
    const loadFavoritesAction = {
      type: ActionType.LOAD_FAVORITES,
      payload: favorites,
    };
    expect(offersData(state, loadFavoritesAction))
      .toEqual({favorites, isFavoritesLoad: true});
  });

  it('should is favorite offer by id', () => {
    const state = {
      offer: convertToCamel(offers[1]),
      offers: convertToCamel(offers),
      favorites: convertToCamel(favorites),
    };
    const loadOffersAction = {
      type: ActionType.OFFER_IS_FAVORITE,
      payload:  convertToCamel(offers[1]),
    };

    const {id, isFavorite} = loadOffersAction.payload;
    const offerById = state.offers.find((hotel) => hotel.id === id);
    offerById.isFavorite = isFavorite;
    if (state.offer) {
      state.offer.isFavorite = isFavorite;
    }
    if(!isFavorite) {
      state.favorites = state.favorites.filter((item) => item.id !== id);
    }
    expect(offersData(state, loadOffersAction))
      .toEqual(state);
  });
});

describe('Reducer: offersNearData', () => {
  it('without additional parameters should return initial state', () => {
    const initialState ={
      offersNear: [],
    };
    expect(offersNearData(undefined, {}))
      .toEqual(initialState);
  });

  it('should load offers near', () => {
    const state = {
      offersNear: [],
    };
    const loadOffersNearAction = {
      type: ActionType.LOAD_NEAR,
      payload: nearby,
    };
    expect(offersNearData(state, loadOffersNearAction))
      .toEqual({offersNear: nearby});
  });
});
