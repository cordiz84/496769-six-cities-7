import {loadNear} from '../action';
import {createReducer} from '@reduxjs/toolkit';

const initialState = {
  offersNear: [],
};

const offersNearData = createReducer(initialState, (builder) => {
  builder
    .addCase(loadNear, (state, action) => {
      state.offersNear = action.payload;
    });
});


export {offersNearData};
