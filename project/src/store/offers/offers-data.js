import {changeCity, changeSortOption, loadFavorites, loadOffers, loadOffersById, offerIsFavorite} from '../action';
import {createReducer} from '@reduxjs/toolkit';
import {Cities, SortOptions} from '../../const';

const initialState = {
  offers: [],
  offer: null,
  favorites: [],
  isDataLoaded: false,
  isDataLoadedById: false,
  isFavoritesLoad: false,
  city: Cities.PARIS.name,
  sortOption: SortOptions.POPULAR,
};

const offersData = createReducer(initialState, (builder) => {
  builder
    .addCase(changeCity, (state, action) => {
      state.city = action.payload;
    })
    .addCase(changeSortOption, (state, action) => {
      state.sortOption = action.payload;
    })
    .addCase(loadOffers, (state, action) => {
      state.offers = [];
      state.offers = action.payload;
      state.isDataLoaded = true;
    })
    .addCase(loadOffersById, (state, action) => {
      state.offer = null;
      state.offer = action.payload;
      state.isDataLoadedById = true;
    })
    .addCase(loadFavorites, (state, action) => {
      state.favorites = [];
      state.favorites = action.payload;
      state.isFavoritesLoad = true;
    })
    .addCase(offerIsFavorite, (state, action) => {
      const {id, isFavorite} = action.payload;
      const offerById = state.offers.find((offer) => offer.id === id);
      Object.assign(offerById, action.payload);

      if (state.offer) {
        state.offer = action.payload;
      }
      if(state.favorites.length > 0) {
        !isFavorite ?
          state.favorites = state.favorites.filter((item) => item.id !== id)
          :
          state.favorites.push(action.payload);
      }

    });
});

export {offersData};
