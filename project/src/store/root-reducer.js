import {combineReducers} from 'redux';
import {offersData} from './offers/offers-data';
import {offersNearData} from './offers/offers-near-data';
import {reviewsData} from './reviews/reviews-data';
import {sortOptions} from './sort-options/sort-options';
import {city} from './city/city';
import {user} from './user/user';
import {errorData} from './error/error';

export const NameSpace = {
  OFFERS_DATA: 'OFFERS_DATA',
  OFFERS_NEAR_DATA: 'OFFERS_NEAR_DATA',
  REVIEWS_DATA: 'REVIEWS_DATA',
  SORT_OPTIONS: 'SORT_OPTIONS',
  CITY: 'CITY',
  USER: 'USER',
  ERROR: 'ERROR',
};

export default combineReducers({
  [NameSpace.OFFERS_DATA]: offersData,
  [NameSpace.OFFERS_NEAR_DATA]: offersNearData,
  [NameSpace.REVIEWS_DATA]: reviewsData,
  [NameSpace.SORT_OPTIONS]: sortOptions,
  [NameSpace.CITY]: city,
  [NameSpace.USER]: user,
  [NameSpace.ERROR]: errorData,
});
