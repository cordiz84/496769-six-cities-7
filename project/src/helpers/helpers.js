import {AuthorizationStatus, SortOptions} from '../const';

export const ratingToPercentage = function (rating) {
  return rating * 100 / 5;
};

const isObject = function (item) {
  return item === Object(item) && !Array.isArray(item) && typeof item !== 'function';
};

const snakeToCamel = (key) => key.replace(/([-_][a-z])/ig, (key1) => key1.toUpperCase()
  .replace('-', '')
  .replace('_', ''));

export const convertToCamel = function (key) {
  if (isObject(key)) {
    const n = {};
    Object.keys(key)
      .forEach((k) => {
        n[snakeToCamel(k)] = convertToCamel(key[k]);
      });
    return n;
  } else if (Array.isArray(key)) {
    return key.map((i) => convertToCamel(i));
  }
  return key;
};

export const toUpFirst = function (name) {
  if (!name) {return name;}
  name=name.toLowerCase();
  return name[0].toUpperCase() + name.slice(1);
};

export const toSortOffers = function (offers, city, sort) {
  const offersSort = offers.filter((offer) => offer.city.name === city);
  switch (sort) {
    case SortOptions.HIGH_TO_LOW : {
      return offersSort.sort((a, b) => b.price - a.price);
    }
    case SortOptions.LOW_TO_HIGH : {
      return offersSort.sort((a, b) => a.price - b.price);
    }
    case SortOptions.RATED_FIRST : {
      return offersSort.sort((a, b) => b.rating - a.rating);
    }
    default:
      return offersSort;
  }
};

export const isCheckedAuth = (authorizationStatus) =>
  authorizationStatus !== AuthorizationStatus.UNKNOWN;


