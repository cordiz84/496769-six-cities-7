import React, {useState} from 'react';
import {login} from '../../../store/api-actions';
import {useDispatch} from 'react-redux';
import Header from '../../header/header';

function SignIn() {
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    email: '',
    password: '',
  });

  const [validate, setValidate] = useState(false);
  const [errors, setErrors] = useState({});

  const validateValue = function (values) {
    const errorValues = {};

    if (!values.password) {
      errorValues.password = 'is required';
    } else if (/\s+/.test(values.password)) {
      errorValues.password = 'Password must not contain spaces';
    }
    if (!values.email) {
      errorValues.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
      errorValues.email = 'Email address is invalid';
    }
    Object.keys(errors).length === 0 ? setValidate(true) : setValidate(false);
    return errorValues;
  };

  const handleChange = function (event) {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
    setErrors(validateValue(formData));
  };
  const handleFormSubmit = function (event) {
    event.preventDefault();
    dispatch(login(formData));
  };
  return (
    <div className="page page--gray page--login">
      <Header/>
      <main className="page__main page__main--login">
        <div className="page__login-container container">
          <section className="login">
            <h1 className="login__title">Sign in</h1>
            <form className="login__form form" action="#" method="post"  onSubmit={handleFormSubmit}>
              <div className="login__input-wrapper form__input-wrapper">
                <label className="visually-hidden">E-mail</label>
                <input className="login__input form__input"  type="email" name="email" onChange={handleChange} placeholder="Email" required=""/>
              </div>
              <div className="login__input-wrapper form__input-wrapper">
                <label className="visually-hidden">Password</label>
                <input className="login__input form__input" type="password" onChange={handleChange} name="password" placeholder="Password"
                  required=""
                />
              </div>
              <button data-testid="submit-button" className="login__submit form__submit button"  disabled={!validate} type="submit">Sign in</button>
            </form>
          </section>
          <section className="locations locations--login locations--current">
            <div className="locations__item">
              <a className="locations__item-link" href={'/'}>
                <span>Amsterdam</span>
              </a>
            </div>
          </section>
        </div>
      </main>
    </div>
  );
}

export default SignIn;
