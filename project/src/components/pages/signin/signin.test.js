import React from 'react';
import { render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import SignIn from './signin';
import userEvent from '@testing-library/user-event';
import * as Redux from 'react-redux';

let history = null;
let store = null;

jest.mock('../../header/header', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Header</div>;
  },
}));

describe('Component: SignIn', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore();
  });


  it('should render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    const fakeFormData = {
      email: 'test@test.ru',
      password: '123456',
    };
    history.push('/login');

    render(
      <Provider store={store}>
        <Router history={history}>
          <SignIn />
        </Router>
      </Provider>,
    );

    expect(screen.getByPlaceholderText('Email')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
    userEvent.type(screen.getByPlaceholderText('Email'),fakeFormData.email);
    userEvent.type(screen.getByPlaceholderText('Password'),fakeFormData.password);
    expect(screen.getByDisplayValue(fakeFormData.email)).toBeInTheDocument();
    expect(screen.getByDisplayValue(fakeFormData.password)).toBeInTheDocument();
    userEvent.click(screen.getByRole('button'));
  });

  it('should form not valid', () => {

    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    const fakeFormData = {
      email: 'test',
      password: '1234 5 6',
    };
    history.push('/login');

    render(
      <Provider store={store}>
        <Router history={history}>
          <SignIn />
        </Router>
      </Provider>,
    );

    userEvent.type(screen.getByPlaceholderText('Email'),fakeFormData.email);
    userEvent.type(screen.getByPlaceholderText('Password'),fakeFormData.password);
  });
});
