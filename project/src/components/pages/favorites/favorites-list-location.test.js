import React from 'react';
import {render, screen} from '@testing-library/react';
import {Route, Router, Switch} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {offersMock} from '../../app/app.mock';
import * as Redux from 'react-redux';
import FavoritesListLocations from './favorites-list-locations';
import userEvent from '@testing-library/user-event';
import {AppRoute} from '../../../const';

let history = null;
let store = null;

jest.mock('../../card/card', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Card</div>;
  },
}));

describe('Component: FavoritesListLocations', () => {
  history = createMemoryHistory();
  const createFakeStore = configureStore({});
  store = createFakeStore();
  const offers = [offersMock[1]];
  const city = 'Paris';

  it('should render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    history.push('/fake');

    render(
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path={AppRoute.MAIN} exact>
              <p>This is mock Main Screen</p>
            </Route>
            <Route>
              <FavoritesListLocations offers={offers}  city={city}/>
            </Route>
          </Switch>
        </Router>
      </Provider>);

    expect(screen.getByText(/This is mock Card/i)).toBeInTheDocument();
    expect(screen.getAllByTestId('location_item')[0]).toBeInTheDocument();
    userEvent.click(screen.getAllByTestId('location_item')[0]);
    expect(dispatch).toBeCalled();
    expect(screen.getByText(/This is mock Main Screen/i)).toBeInTheDocument();

  });
});
