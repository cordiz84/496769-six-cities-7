import React, {useEffect} from 'react';
import Footer from '../../footer/footer';
import Header from '../../header/header';
import FavoritesList from './favorites-list';
import {useDispatch, useSelector} from 'react-redux';
import {getFavorites, getIsFavoritesLoad} from '../../../store/offers/selectors';
import {fetchFavoritesList} from '../../../store/api-actions';
import LoadingSpinner from '../../loading-spinner/loading-spinner';
import FavoritesEmpty from './favorites-empty';

function Favorites () {
  const offers = useSelector(getFavorites);
  const dispatch = useDispatch();
  const isDataLoaded = useSelector(getIsFavoritesLoad);

  useEffect(() => {
    dispatch(fetchFavoritesList());
  }, [dispatch] );

  if (!isDataLoaded) {
    return (
      <LoadingSpinner />
    );
  }
  if (offers.length > 0) {
    return (
      <div className="page">
        <Header/>
        <main className="page__main page__main--favorites">
          <div className="page__favorites-container container">
            <section className="favorites">
              <h1 className="favorites__title">Saved listing</h1>
              <FavoritesList offers={offers}/>
            </section>
          </div>
        </main>
        <Footer/>
      </div>);
  }
  return (
    <div className="page page--favorites-empty">
      <Header/>
      <FavoritesEmpty />
      <Footer/>
    </div>
  );
}

export default Favorites;
