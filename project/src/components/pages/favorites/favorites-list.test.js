import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {offersMock} from '../../app/app.mock';
import FavoritesList from './favorites-list';

let history = null;
let store = null;

jest.mock('./favorites-list-locations', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Favorites list location</div>;
  },
}));

describe('Component: FavoritesList', () => {
  history = createMemoryHistory();
  const createFakeStore = configureStore({});
  store = createFakeStore();
  const offers = [offersMock[1]];

  it('should render correctly', () => {
    render(
      <Provider store={store}>
        <Router history={history}>
          <FavoritesList offers={offers}/>
        </Router>
      </Provider>,
    );
    expect(screen.getByText(/This is mock Favorites list location/i)).toBeInTheDocument();
  });
});
