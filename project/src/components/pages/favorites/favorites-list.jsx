import React from 'react';
import FavoritesListLocations from './favorites-list-locations';
import PropTypes from 'prop-types';
import cardProp from '../../card/card.prop';

function FavoritesList({offers}) {
  const cities =  [...new Set(offers.map((city) => city.city.name))];
  return (
    <ul data-testid="favorites-list" className="favorites__list">
      {cities.map((city) =>
        <FavoritesListLocations offers={offers} city={city} key={city}/>,
      )}

    </ul>
  );
}

FavoritesList.propTypes = {
  offers: PropTypes.arrayOf(cardProp),
};

export default FavoritesList;
