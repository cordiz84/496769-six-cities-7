import React from 'react';
import {render} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import FavoritesEmpty from './favorites-empty';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';

const mockStore = configureStore({});

describe('Component: FavoritesEmpty', () => {
  it('should render correctly', () => {
    const history = createMemoryHistory();
    const {getByText} = render(
      <Provider store={mockStore({})}>
        <Router history={history}>
          <FavoritesEmpty />
        </Router>
      </Provider>,
    );
    const headerElement = getByText('Favorites (empty)');
    const statusElement = getByText('Nothing yet saved.');
    const descriptionElement = getByText('Nothing yet saved.');

    expect(headerElement).toBeInTheDocument();
    expect(statusElement).toBeInTheDocument();
    expect(descriptionElement).toBeInTheDocument();
  });
});
