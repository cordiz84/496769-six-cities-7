import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {AuthorizationStatus} from '../../../const';
import {offersMock} from '../../app/app.mock';
import * as Redux from 'react-redux';
import Favorites from './favorites';

let history = null;
let store = null;
let fakeApp = null;

jest.mock('../../header/header', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Header</div>;
  },
}));

jest.mock('../../loading-spinner/loading-spinner', () => ({
  __esModule: true,
  default() {
    return <div>This is mock LoadingSpinner</div>;
  },
}));

const mockDataStore = {
  USER: {
    authorizationStatus: AuthorizationStatus.AUTH,
  },
  OFFERS_DATA: {
    favorites: [offersMock[1]],
    isFavoritesLoad: true,
  },

};
describe('Component: Favorites', () => {
  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});

    store = createFakeStore(mockDataStore);

    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Favorites />
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    render(fakeApp);
    expect(screen.getByText(/Saved listing/i)).toBeInTheDocument();

  });

  it('should render Favorites empty', () => {
    mockDataStore.OFFERS_DATA.favorites = [];
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    render(fakeApp);
    expect(screen.getByText(/Nothing yet saved./i)).toBeInTheDocument();

  });

  it('should render Favorites not load', () => {
    mockDataStore.OFFERS_DATA.isFavoritesLoad=false;
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    render(fakeApp);
    expect(screen.getByText('This is mock LoadingSpinner')).toBeInTheDocument();

  });


});
