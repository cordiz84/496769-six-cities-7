import React from 'react';
import Card from '../../card/card';
import {AppRoute, TypeCard} from '../../../const';
import PropTypes from 'prop-types';
import cardProp from '../../card/card.prop';
import {useDispatch} from 'react-redux';
import {changeCity} from '../../../store/action';
import {Link} from 'react-router-dom';

function FavoritesListLocations({offers, city}) {
  const offersCity = offers.filter((offer) => offer.city.name === city);

  const dispatch = useDispatch();

  const onChangeCity = function (event) {
    dispatch(changeCity(event.target.innerText));
  };

  return (

    <li className="favorites__locations-items">
      <div className="favorites__locations locations locations--current">
        <div className="locations__item">
          <Link data-testid="location_item" className="locations__item-link" to={AppRoute.MAIN} onClick={onChangeCity}>
            <span>{city}</span>
          </Link>
        </div>
      </div>
      <div className="favorites__places">
        {offersCity.map((offer) =>
          <Card offer={offer} key={offer.id} typeCard={TypeCard.FAVORITES}/>,
        )}
      </div>
    </li>
  );

}

FavoritesListLocations.propTypes = {
  city: PropTypes.string.isRequired,
  offers: PropTypes.arrayOf(cardProp),
};

export default FavoritesListLocations;
