import React, {useEffect} from 'react';
import Header from '../../header/header';
import ListCities from './list-cities/list-cities';
import {useSelector} from 'react-redux';
import {getCity} from '../../../store/offers/selectors';
import {getError} from '../../../store/error/selectors';
import toast from 'react-hot-toast';

function MainEmpty() {
  const city = useSelector(getCity);

  const errorMessage= useSelector(getError);
  const notify = () => toast.error(errorMessage, {duration: 5000, position: 'bottom-center'});

  useEffect(() => {
    if (errorMessage) {
      notify();
    }
  });

  return (
    <div className="page page--gray page--main">
      <Header/>
      <main className="page__main page__main--index page__main--index-empty">
        <h1 className="visually-hidden">Cities</h1>
        <ListCities/>
        <div className="cities">
          <div className="cities__places-container cities__places-container--empty container">
            <section className="cities__no-places">
              <div className="cities__status-wrapper tabs__content">
                <b className="cities__status">No places to stay available</b>
                <p className="cities__status-description">We could not find any property available at the moment in {city}
                </p>
              </div>
            </section>
            <div className="cities__right-section"/>
          </div>
        </div>
      </main>
    </div>
  );
}

export default MainEmpty;
