import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import Main from './main';
import {offersMock} from '../../app/app.mock';
import userEvent from '@testing-library/user-event';

let history = null;
let store = null;
let fakeData = null;
const mockDataStore = {
  OFFERS_DATA: {
    offers: offersMock,
    offer: null,
    favorites: [],
    isDataLoaded: true,
    isDataLoadedById: false,
    isFavoritesLoad: false,
    city: 'Paris',
    sortOption: 'Popular',
  },
  USER: {
    authorizationStatus: 'NO_AUTH',
    user: null,
  },
};

jest.mock('../../header/header', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Header</div>;
  },
}));

jest.mock('../../map/map', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Map</div>;
  },
}));

jest.mock('./sorting-options/sorting-options', () => ({
  __esModule: true,
  default() {
    return <div>This is mock SortingOptions</div>;
  },
}));

jest.mock('./list-cities/list-cities', () => ({
  __esModule: true,
  default() {
    return <div>This is mock ListCities</div>;
  },
}));

jest.mock('../../card/card', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Card</div>;
  },
}));


describe('Component: Main', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore(mockDataStore);
    fakeData = (
      <Provider store={store}>
        <Router history={history}>
          <Main />
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    render(fakeData);

    expect(screen.getByText(/Header/i)).toBeInTheDocument();
    expect(screen.getByText(/ListCities/i)).toBeInTheDocument();
    expect(screen.getByText(/places to stay in/i)).toBeInTheDocument();
    expect(screen.getByText(/SortingOptions/i)).toBeInTheDocument();
    expect(screen.getAllByText(/Card/i)).toHaveLength(offersMock.length);
    expect(screen.getByText(/Map/i)).toBeInTheDocument();
    userEvent.hover(screen.getAllByText(/Card/i)[0]);

  });
});
