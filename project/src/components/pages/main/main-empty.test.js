import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import configureStore from 'redux-mock-store';
import MainEmpty from './main-empty';
import {Provider} from 'react-redux';
import {AuthorizationStatus} from '../../../const';

let store;
describe('Component: MainEmpty', () => {
  it('should render correctly', () => {
    const history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore({
      OFFERS_DATA: {
        city: 'Paris',
      },
      USER: {
        authorizationStatus: AuthorizationStatus.NO_AUTH,
      },
      ERROR: {
        errors: null,
      },
    });
    render(
      <Provider store={store}>
        <Router history={history}>
          <MainEmpty />
        </Router>
      </Provider>,
    );

    expect(screen.getByText('No places to stay available')).toBeInTheDocument();

  });
});
