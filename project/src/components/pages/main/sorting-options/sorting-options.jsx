import React, {useState} from 'react';
import {changeSortOption} from '../../../../store/action';
import {useDispatch, useSelector} from 'react-redux';
import {SortOptions} from '../../../../const';
import {getSortOffers} from '../../../../store/offers/selectors';

function SortingOptions() {

  const sort = useSelector(getSortOffers);
  const dispatch = useDispatch();

  const [isActive, setActive] = useState(false);

  const propMapSort = {
    [SortOptions.POPULAR]: {
      name: 'Popular',
    },
    [SortOptions.LOW_TO_HIGH]: {
      name: 'Price: low to high',
    },
    [SortOptions.HIGH_TO_LOW]: {
      name: 'Price: high to low',
    },
    [SortOptions.RATED_FIRST]: {
      name: 'Top rated first',
    },
  };
  const options = Object.values(SortOptions);

  const onChangeSortOption = function (event) {
    event.preventDefault();
    dispatch(changeSortOption(event.target.dataset.key));
    setActive(!isActive);
  };

  return (
    <form className="places__sorting" action={'/'} method="get">
      <span className="places__sorting-caption">Sort by </span>
      <span data-testid="sorting_type" className="places__sorting-type" tabIndex="0" onClick={() => setActive(!isActive)}>
        {propMapSort[sort].name}
        <svg className="places__sorting-arrow" width="7" height="4">
          <use xlinkHref="#icon-arrow-select"/>
        </svg>
      </span>
      <ul className={`places__options places__options--custom places__options${isActive && '--opened'}`}>
        {options.map((option, index) =>
          <li data-testid="option" className="places__option" onClick={onChangeSortOption} tabIndex={index} data-key={option} key={option} >{propMapSort[option].name}</li>,
        )}
      </ul>
    </form>
  );
}

export default SortingOptions;
