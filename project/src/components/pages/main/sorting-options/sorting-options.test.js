import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import * as Redux from 'react-redux';
import SortingOptions from './sorting-options';
import userEvent from '@testing-library/user-event';

let history = null;
let store = null;
let fakeData = null;
const mockDataStore = {
  OFFERS_DATA: {
    sortOption: 'Popular',
  },
};

describe('Component: SortOptions', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore(mockDataStore);
    fakeData = (
      <Provider store={store}>
        <Router history={history}>
          <SortingOptions />
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    render(fakeData);
    expect(screen.getByText('Sort by')).toBeInTheDocument();
  });

  it('should change sort option', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    render(fakeData);

    expect(screen.getByTestId('sorting_type')).toBeInTheDocument();
    userEvent.click(screen.getByTestId('sorting_type'));
    expect(screen.getAllByTestId('option')[3]).toBeInTheDocument();
    userEvent.click(screen.getAllByTestId('option')[3]);
    expect(dispatch).toBeCalledWith({payload: 'ratedFirst', type: 'SORT_OPTION'});
  });
});
