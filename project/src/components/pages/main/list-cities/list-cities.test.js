import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import * as Redux from 'react-redux';
import userEvent from '@testing-library/user-event';
import ListCities from './list-cities';
import {Cities} from '../../../../const';

let history = null;
let store = null;
const mockDataStore = {
  OFFERS_DATA: {
    city: 'Paris',
  },
};
describe('Component: ListCities', () => {
  history = createMemoryHistory();
  const createFakeStore = configureStore({});
  store = createFakeStore(mockDataStore);

  it('should render correctly & location click event', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    history.push('/fake');

    render(
      <Provider store={store}>
        <Router history={history}>
          <ListCities />
        </Router>
      </Provider>,
    );

    expect(screen.getAllByTestId('location_item')[0]).toBeInTheDocument();
    userEvent.click(screen.getAllByTestId('location_item')[1]);
    expect(dispatch).toBeCalled();
    expect(screen.getAllByTestId('location_item')).toHaveLength(Object.keys(Cities).length);

  });
});
