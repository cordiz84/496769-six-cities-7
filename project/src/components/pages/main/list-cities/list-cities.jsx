import React, {memo} from 'react';
import {changeCity} from '../../../../store/action';
import {useDispatch, useSelector} from 'react-redux';
import {Cities} from '../../../../const';
import {toUpFirst} from '../../../../helpers/helpers';
import {getCity} from '../../../../store/offers/selectors';

function ListCities() {
  const cities = Object.keys(Cities).map((city) => toUpFirst(city));
  const activeCity = useSelector(getCity);
  const dispatch = useDispatch();

  const onChangeCity = function (event) {
    event.preventDefault();
    dispatch(changeCity(event.target.innerText));
  };

  return (
    <div className="tabs">
      <section className="locations container">
        <ul className="locations__list tabs__list">
          {cities.map((city) => (
            <li className="locations__item" key={city}>
              <a data-testid="location_item" className={`locations__item-link tabs__item${city === activeCity ? '--active' : ''}`} href={'/'}
                onClick={onChangeCity}
              >
                <span>{city}</span>
              </a>
            </li>),
          )}
        </ul>
      </section>
    </div>
  );
}

export default memo(ListCities);
