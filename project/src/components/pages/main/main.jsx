import React, {useState} from 'react';
import Card from '../../card/card';
import Header from '../../header/header';
import ListCities from './list-cities/list-cities';
import Map from '../../map/map';
import {useSelector} from 'react-redux';
import SortingOptions from './sorting-options/sorting-options';
import {TypeCard} from '../../../const';
import {getCity, getCitySortOffers} from '../../../store/offers/selectors';

function Main () {
  const [isActiveCard, setActiveCard] = useState(null);
  const city = useSelector(getCity);
  const offers = useSelector(getCitySortOffers);
  const cityCount = offers.length;

  return (
    <div className="page page--gray page--main">
      <Header/>

      <main className="page__main page__main--index">
        <h1 className="visually-hidden">Cities</h1>
        <ListCities/>
        <div className="cities">
          <div className="cities__places-container container">
            <section className="cities__places places">
              <h2 className="visually-hidden">Places</h2>
              <b className="places__found">{cityCount} places to stay in {city}</b>
              <SortingOptions />
              <div className="cities__places-list places__list tabs__content">
                {offers.map((offer) =>
                  <Card hoverHandler={(card) => setActiveCard(card)} offer={offer} typeCard={TypeCard.CITIES} key={offer.id}/>,
                )}
              </div>
            </section>
            <div className="cities__right-section">
              <section className="cities__map map">
                <Map
                  isActiveCard={isActiveCard}
                  offers={offers}
                  city={city}
                />
              </section>
            </div>
          </div>
        </div>
      </main>
    </div>);
}

export default  Main;
