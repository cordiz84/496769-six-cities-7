import React from 'react';
import {render, screen} from '@testing-library/react';
import {createMemoryHistory} from 'history';
import configureStore from 'redux-mock-store';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import * as Redux from 'react-redux';

import Room from './room';
import {offersMock} from '../../app/app.mock';
import {ratingToPercentage, toUpFirst} from '../../../helpers/helpers';
import userEvent from '@testing-library/user-event';

let history = null;
let store = null;
let fakeData = null;
const mockDataStore = {
  OFFERS_DATA: {
    offer: offersMock[0],
    isDataLoadedById: true,
  },
  OFFERS_NEAR_DATA: {
    offersNear: [{id:1},{id:2},{id:3},{id:4}],
  },
  USER: {
    authorizationStatus: 'AUTH',
  },
};

jest.mock('../../header/header', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Header</div>;
  },
}));

jest.mock('../../map/map', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Map</div>;
  },
}));

jest.mock('../../loading-spinner/loading-spinner', () => ({
  __esModule: true,
  default() {
    return <div>This is mock LoadingSpinner</div>;
  },
}));

jest.mock('./review/review-list', () => ({
  __esModule: true,
  default() {
    return <div>This is mock ReviewList</div>;
  },
}));

jest.mock('../../card/card', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Card</div>;
  },
}));

describe('Component: Room', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore(mockDataStore);
    const offerId = '1';

    fakeData = (
      <Provider store={store}>
        <Router history={history}>
          <Room offerId={offerId}/>
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    const {title, price, rating, type, bedrooms, maxAdults,
      goods, host, description, images} = mockDataStore.OFFERS_DATA.offer;
    render(fakeData);
    expect(screen.getByText(title)).toBeInTheDocument();
    expect(screen.getByText(description)).toBeInTheDocument();
    expect(screen.getByText(`€${price}`)).toBeInTheDocument();
    expect(screen.getByText(toUpFirst(type))).toBeInTheDocument();
    expect(screen.getByText(`${bedrooms} Bedrooms`)).toBeInTheDocument();
    expect(screen.getByText(`Max ${maxAdults} adults`)).toBeInTheDocument();
    expect(screen.getByTestId('rating')).toHaveAttribute('style', `width: ${ratingToPercentage(rating)}%;`);
    expect(screen.getAllByText('This is mock Card')).toHaveLength(3);
    expect(screen.getAllByTestId('good-item')).toHaveLength(goods.length);
    expect(screen.getAllByTestId('image-item')).toHaveLength(images.length);
    expect(screen.getByTestId('user-avatar')).toHaveAttribute('src', host.avatarUrl);
    expect(screen.getByTestId('to-bookmark')).not.toHaveClass('property__bookmark-button--active');

  });

  it('should is favorited & is Premium', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    const {offer} = mockDataStore.OFFERS_DATA;
    offer.isPremium = true;
    offer.isFavorite = true;

    render(fakeData);
    expect(screen.getByText('Premium')).toBeInTheDocument();
    expect(screen.getByTestId('to-bookmark')).toHaveClass('property__bookmark-button--active');
    userEvent.click(screen.getByTestId('to-bookmark'));
    expect(screen.getByTestId('to-bookmark')).toHaveClass('property__bookmark-button--active');
    expect(dispatch).toHaveBeenCalledTimes(3);

  });

  it('should not load', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    mockDataStore.OFFERS_DATA.isDataLoadedById = false;
    // mockDataStore.OFFERS_DATA.offer = null;

    render(fakeData);
    expect(screen.getByText('This is mock LoadingSpinner')).toBeInTheDocument();

  });

  it('should is not fount', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    mockDataStore.OFFERS_DATA.isDataLoadedById = true;
    mockDataStore.OFFERS_DATA.offer = null;

    render(fakeData);
    expect(screen.getByText('404. Page not found')).toBeInTheDocument();

  });
});
