import React from 'react';
import {render, screen} from '@testing-library/react';
import Review from './review';
import {ratingToPercentage} from '../../../../helpers/helpers';
import moment from 'moment';

describe('Component: Review', () => {
  it('should render correctly', () => {
    const review = {
      id: 1,
      comment: 'lorem ipsum dolor sit amet',
      date: '2021-06-30T16:51:35.215Z',
      rating: 3,
      user: {
        id: 1,
        name: 'Fake Ivan',
        avatarUrl: 'img/fake-avatar.jpg',
        isPro: true,
      },
    };
    render(
      <Review review={review}/>,
    );
    const  {comment, date, rating, user} = review;
    expect(screen.getByText('Rating')).toBeInTheDocument();
    expect(screen.getByRole('img')).toHaveAttribute('src', user.avatarUrl);
    expect(screen.getByText(user.name)).toBeInTheDocument();
    expect(screen.getByTestId('rating')).toHaveStyle(`width: ${ratingToPercentage(rating)}%`);
    expect(screen.getByText(comment)).toBeInTheDocument();
    expect(screen.getByTestId('review_time')).toHaveAttribute('datetime', moment(date).format('DD.MM.YYYY'));
  });
});
