import React, {useEffect} from 'react';
import Review from './review';
import ReviewForm from './review-form';
import {AuthorizationStatus} from '../../../../const';
import {useDispatch, useSelector} from 'react-redux';
import PropTypes from 'prop-types';
import {fetchReviewList} from '../../../../store/api-actions';
import {getAuthorizationStatus} from '../../../../store/user/selectors';
import {getReviewsSort} from '../../../../store/reviews/selectors';

function ReviewList({hotelId}) {
  const authorizationStatus= useSelector(getAuthorizationStatus);
  const reviews= useSelector(getReviewsSort);
  const dispatch = useDispatch();

  const REVIEW_COUNT = reviews.length;
  const isAuthorization = AuthorizationStatus.AUTH === authorizationStatus;

  useEffect(() => {
    dispatch(fetchReviewList(hotelId));
  }, [hotelId, dispatch],
  );

  return (
    <section className="property__reviews reviews">
      <h2 className="reviews__title">Reviews &middot; <span className="reviews__amount">{REVIEW_COUNT}</span></h2>
      <ul className="reviews__list">
        {reviews.slice(0,10).map((review) =>
          <Review review={review} key={review.id}/>,
        )}
      </ul>
      {isAuthorization && <ReviewForm hotelId={hotelId}/>}
    </section>
  );
}

ReviewList.propTypes = {
  hotelId: PropTypes.number.isRequired,
};

export default ReviewList;
