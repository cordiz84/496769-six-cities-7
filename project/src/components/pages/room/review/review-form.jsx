import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useDispatch, useSelector} from 'react-redux';
import ReviewStar from './review-star';
import {createReview} from '../../../../store/api-actions';
import {getError} from '../../../../store/error/selectors';

function ReviewForm({hotelId}) {
  const errorApp = useSelector(getError);
  const dispatch = useDispatch();

  const [comment, setComment] = useState({ hotelId: hotelId,
    review: '',
    rating: 0,
  });
  const [errors, setErrors] = useState({});

  const [disabled, setDisabled] = useState(false);

  const isValidate = !!Object.keys(errors).length;


  const stars = [{id:'1', title: 'terribly'}, {id:'2', title: 'badly'},{id:'3', title: 'not bad'},
    {id:'4', title: 'good'}, {id:'5', title: 'perfect'}];

  const validateValue = function (values) {
    const errorValues = {};
    if (values.review.length < 50 || values.review.length > 300) {
      errorValues.review = 'Review should be 50 to 300 characters';
    }
    if (values.rating === 0 ) {
      errorValues.rating = 'Minimum rating 1';
    }
    return errorValues;
  };

  const handleChange = function (event) {
    setComment({
      ...comment,
      [event.target.name]: event.target.value,
    });
    setErrors(validateValue(comment));
  };

  const handleFormSubmit = function (event) {
    event.preventDefault();
    setDisabled(true);
    dispatch(createReview(comment));
    if (!errorApp) {
      setComment(Object.assign(comment, {review: '', rating: 0}));
    }
    setDisabled(false);
  };

  return (
    <form className="reviews__form form" onSubmit={handleFormSubmit}>
      <label className="reviews__label form__label" htmlFor="review">Your review</label>
      <div data-testid="rating"  className="reviews__rating-form form__rating" >
        {
          stars.sort((a,b) => b.id-a.id).map((star) => (
            <ReviewStar handleChange={handleChange} disabled={disabled}  star={star} isDisabled={disabled} rating={comment.rating.toString()} key={star.id}/>
          ))
        }
      </div>
      <textarea disabled={disabled} className="reviews__textarea form__textarea"
        id="review" name="review" onChange={handleChange} value={comment.review || ''}
        placeholder="Tell how was your stay, what you like and what can be improved"
      />
      <span className={!errors && !errorApp ? 'visually-hidden': ''} style={{color: '#ff1744'}}>{errors.review} {errors.rating} {errorApp}</span>
      <div className="reviews__button-wrapper">
        <p className="reviews__help">
          To submit review please make sure to set <span className="reviews__star">rating</span> and
          describe your stay with at least <b className="reviews__text-amount">50 characters</b>.
        </p>
        <button className="reviews__submit form__submit button" type="submit" disabled={isValidate || disabled}>Submit</button>
      </div>
    </form>
  );
}
ReviewForm.propTypes = {
  comment: PropTypes.shape({
    rating: PropTypes.number.isRequired,
    review: PropTypes.string.isRequired,
  }),
  hotelId: PropTypes.number.isRequired,
};

export default ReviewForm;
