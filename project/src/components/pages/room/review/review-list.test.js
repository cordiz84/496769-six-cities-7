import React from 'react';
import {render, screen} from '@testing-library/react';
import {createMemoryHistory} from 'history';
import configureStore from 'redux-mock-store';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import * as Redux from 'react-redux';
import ReviewList from './review-list';
import {AuthorizationStatus} from '../../../../const';

let history = null;
let store = null;
let fakeData = null;
const mockDataStore = {
  REVIEWS_DATA: {
    reviews: [{id:1}, {id:2}],
  },
  USER: {
    authorizationStatus: AuthorizationStatus.NO_AUTH,
    user: null,
  },
};

jest.mock('./review', () => ({
  __esModule: true,
  default() {
    return <div>This is mock Review</div>;
  },
}));

jest.mock('./review-form', () => ({
  __esModule: true,
  default() {
    return <div>This is mock ReviewForm</div>;
  },
}));

describe('Component: ReviewList', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore(mockDataStore);
    const hotelId = 1;

    fakeData = (
      <Provider store={store}>
        <Router history={history}>
          <ReviewList hotelId={hotelId}/>
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    const {reviews} = mockDataStore.REVIEWS_DATA;
    render(fakeData);
    expect(screen.getByText('Reviews ·')).toBeInTheDocument();
    expect(screen.getAllByText('This is mock Review')).toHaveLength(reviews.length);
  });

  it('should review form render correctly', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    mockDataStore.USER.authorizationStatus =  AuthorizationStatus.AUTH;

    render(fakeData);
    expect(screen.getByText('This is mock ReviewForm')).toBeInTheDocument();
  });
});
