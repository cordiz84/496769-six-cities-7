import React from 'react';
import {render, screen} from '@testing-library/react';
import ReviewForm from './review-form';
import {createMemoryHistory} from 'history';
import configureStore from 'redux-mock-store';
import {Router} from 'react-router-dom';
import {Provider} from 'react-redux';
import * as Redux from 'react-redux';
import userEvent from '@testing-library/user-event';

let history = null;
let store = null;
let fakeData = null;

describe('Component: ReviewFrom', () => {

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});
    store = createFakeStore({
      ERROR: {
        error: null,
      },
    });
    const hotelId = 1;
    fakeData = (
      <Provider store={store}>
        <Router history={history}>
          <ReviewForm hotelId={hotelId}/>
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    render(fakeData);
    expect(screen.getByText('Your review')).toBeInTheDocument();
    expect(screen.getByPlaceholderText(/Tell how was your stay/i)).toBeInTheDocument();
  });

  it('should submit form', () => {
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    const fakeComment = 'Lorem ipsum dolor sit amet, consecrated .Lorem ipsum dolor';

    render(fakeData);

    userEvent.click(screen.getAllByRole('radio')[2]);
    expect(screen.getAllByRole('radio')[2].value).toBe('3');
    userEvent.type(screen.getByRole('textbox'), fakeComment);
    expect(screen.getByRole('textbox').value).toEqual(fakeComment);
    userEvent.click(screen.getByRole('button', { name: /submit/i }));
    expect(dispatch).toBeCalled();
  });
});
