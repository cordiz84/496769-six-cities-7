import React from 'react';

import PropTypes from 'prop-types';

function ReviewStar({handleChange, star, isDisabled, rating}) {
  const {id, title} = star;
  const isRating = rating === id;

  return (
    <>
      <input onChange={handleChange} disabled={isDisabled} className="form__rating-input visually-hidden" name="rating"  checked={isRating} value={id} id={`${id}-stars`}
        type="radio"
      />
      <label  htmlFor={`${id}-stars`} className="reviews__rating-label form__rating-label" title={title}>
        <svg className="form__star-image" width="37" height="33">
          <use xlinkHref="#icon-star"/>
        </svg>
      </label>
    </>
  );
}

ReviewStar.propTypes = {
  star: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
  isDisabled: PropTypes.bool.isRequired,
  rating: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default ReviewStar;
