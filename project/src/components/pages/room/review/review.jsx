import React from 'react';
import {ratingToPercentage} from '../../../../helpers/helpers';
import ReviewProp from './review.prop';
import moment from 'moment';

function Review({review}) {
  const  {comment, date, rating, user} = review;

  return (
    <li className="reviews__item">
      <div className="reviews__user user">
        <div className="reviews__avatar-wrapper user__avatar-wrapper">
          <img className="reviews__avatar user__avatar" src={user.avatarUrl} width="54" height="54"
            alt={user.name}
          />
        </div>
        <span className="reviews__user-name">
          {user.name}
        </span>
      </div>
      <div className="reviews__info">
        <div className="reviews__rating rating">
          <div className="reviews__stars rating__stars">
            <span data-testid="rating" style={{width: `${ratingToPercentage(rating)}%`}}/>
            <span className="visually-hidden">Rating</span>
          </div>
        </div>
        <p className="reviews__text">
          {comment}
        </p>
        <time data-testid="review_time" className="reviews__time" dateTime={moment(date).format('DD.MM.YYYY')}>
          {moment(date).format('MMMM YYYY')}
        </time>
      </div>
    </li>
  );
}

Review.propTypes = {
  review: ReviewProp,
};
export default Review;
