import React, {useEffect} from 'react';
import Header from '../../header/header';
import {ratingToPercentage, toUpFirst} from '../../../helpers/helpers';
import PropTypes from 'prop-types';
import Card from '../../card/card';
import Map from '../../map/map';
import ReviewList from './review/review-list';
import {AppRoute, AuthorizationStatus, TypeCard} from '../../../const';
import {useDispatch, useSelector} from 'react-redux';
import {addIsFavorites, fetchOffersById, fetchOffersNearbyList} from '../../../store/api-actions';
import {getIsDataLoadedById, getOfferById, getOffersNearMap} from '../../../store/offers/selectors';
import LoadingSpinner from '../../loading-spinner/loading-spinner';
import NotFound from '../not-found/not-found';
import {getAuthorizationStatus} from '../../../store/user/selectors';
import {useHistory} from 'react-router-dom';

function Room({offerId}) {
  const offer = useSelector(getOfferById);
  const nearOffers = useSelector(getOffersNearMap);
  const dispatch = useDispatch();
  const isDataLoaded= useSelector(getIsDataLoadedById);
  const authorizationStatus= useSelector(getAuthorizationStatus);
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchOffersById(offerId));
    dispatch(fetchOffersNearbyList(offerId));
  }, [dispatch, offerId] );

  if (!isDataLoaded) {
    return (
      <LoadingSpinner />
    );
  }
  if (offer) {
    const {id,  title, price, rating, type, isPremium, isFavorite,
      bedrooms, maxAdults, goods, host, description, images} = offer;

    const handleFavorite = function (event) {
      event.preventDefault();
      if (authorizationStatus === AuthorizationStatus.AUTH) {
        dispatch(addIsFavorites(offerId, !isFavorite));
      } else {
        history.push(AppRoute.SIGN_IN);
      }
    };

    return (
      <div className="page">
        <Header/>

        <main className="page__main page__main--property">
          <section className="property">
            <div className="property__gallery-container container">
              <div className="property__gallery">
                {images.slice(0,6).map((image) =>(
                  <div className="property__image-wrapper" key={image}>
                    <img data-testid="image-item" className="property__image" src={image} alt="Photo_studio"/>
                  </div>),
                )}
              </div>
            </div>
            <div className="property__container container">
              <div className="property__wrapper">
                {isPremium &&
                <div className="property__mark">
                  <div className="place-card__mark">
                    <span>Premium</span>
                  </div>
                </div>}
                <div className="property__name-wrapper">
                  <h1 className="property__name">
                    {title}
                  </h1>
                  <button data-testid="to-bookmark" className={`${isFavorite ? 'property__bookmark-button--active' : ''} property__bookmark-button button`}
                    type="button"
                    onClick={handleFavorite}
                  >
                    <svg className="property__bookmark-icon" width="31" height="33">
                      <use xlinkHref="#icon-bookmark"/>
                    </svg>
                    <span className="visually-hidden">To bookmarks </span>
                  </button>
                </div>
                <div className="property__rating rating">
                  <div className="property__stars rating__stars">
                    <span data-testid="rating" style={{width: `${ratingToPercentage(rating)}%`}}/>
                    <span className="visually-hidden">Rating</span>
                  </div>
                  <span className="property__rating-value rating__value">{rating}</span>
                </div>
                <ul className="property__features">
                  <li className="property__feature property__feature--entire">
                    {toUpFirst(type)}
                  </li>
                  <li className="property__feature property__feature--bedrooms">
                    {bedrooms} Bedrooms
                  </li>
                  <li className="property__feature property__feature--adults">
                    Max {maxAdults} adults
                  </li>
                </ul>
                <div className="property__price">
                  <b className="property__price-value">&euro;{price}</b>
                  <span className="property__price-text">&nbsp;night</span>
                </div>
                <div className="property__inside">
                  <h2 className="property__inside-title">What&apos;s inside</h2>
                  <ul className="property__inside-list">
                    {goods.map((good) =>(<li data-testid="good-item" className="property__inside-item" key={good}>{good}</li>))}
                  </ul>
                </div>
                <div className="property__host">
                  <h2 className="property__host-title">Meet the host</h2>
                  <div className="property__host-user user">
                    <div className="property__avatar-wrapper property__avatar-wrapper--pro user__avatar-wrapper">
                      <img data-testid="user-avatar" className="property__avatar user__avatar" src={host.avatarUrl} width="74"
                        height="74" alt="Host avatar"
                      />
                    </div>
                    <span className="property__user-name">
                      {host.name}
                    </span>
                    <span className="property__user-status">
                      {host.isPro && 'Pro'}
                    </span>
                  </div>
                  <div className="property__description">
                    <p className="property__text">
                      {description}
                    </p>
                  </div>
                </div>
                <ReviewList hotelId={id}/>
              </div>
            </div>
            <section className="property__map map">
              <Map
                isActiveCard={offer}
                offers={nearOffers}
                city={offer.city.name}
              />
            </section>
          </section>
          <div className="container">
            <section className="near-places places">
              <h2 className="near-places__title">Other places in the neighbourhood</h2>
              <div className="near-places__list places__list">
                {nearOffers.slice(0,3).map((item) =>
                  (
                    <Card offer={item} typeCard={TypeCard.NEAR} key={item.id}/>
                  ),
                )}
              </div>
            </section>
          </div>
        </main>
      </div>
    );

  }

  return (
    <NotFound />
  );
}

Room.propTypes = {
  offerId: PropTypes.string.isRequired,
};

export default Room;
