import React from 'react';
import {render} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import FailedLoadData from './failed-load-data';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';

let history = null;
let store = null;
describe('Component: FailedLoadData', () => {
  history = createMemoryHistory();
  const createFakeStore = configureStore({});
  store = createFakeStore({
    ERROR: {
      error: 'error',
    },
  });

  it('should render correctly', () => {
    const {getByText} = render(
      <Provider store={store}>
        <Router history={history}>
          <FailedLoadData />
        </Router>
      </Provider>,
    );
    const headerElement = getByText('Error Page Load');
    const linkElement = getByText('Go back to the main page');
    const errorMessage = getByText('error');

    expect(headerElement).toBeInTheDocument();
    expect(linkElement).toBeInTheDocument();
    expect(errorMessage).toBeInTheDocument();
  });
});
