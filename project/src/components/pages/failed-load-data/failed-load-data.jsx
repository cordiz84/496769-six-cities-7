import React from 'react';
import {Link} from 'react-router-dom';
import {useSelector} from 'react-redux';
import {getError} from '../../../store/error/selectors';

function FailedLoadData() {
  const errorMessage= useSelector(getError);

  return (
    <div style={{textAlign: 'center'}}>
      <h1>Error Page Load</h1>
      <h3 style={{color: '#d50000'}}>{errorMessage}</h3>
      <Link to="/">Go back to the main page</Link>
    </div>
  );
}

export default FailedLoadData;
