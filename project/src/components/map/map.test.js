import React from 'react';
import {render} from '@testing-library/react';
import * as useMap from '../../hooks/use-map.js';
import Map from './map';
import {offersMock} from '../app/app.mock';
import {Provider} from 'react-redux';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import configureStore from 'redux-mock-store';
import {Cities} from '../../const';
import {toSortOffers} from '../../helpers/helpers';

let history = null;
let store = null;
const fakeDataStore = {
  OFFERS_DATA: {
    offers: offersMock,
  },
  OFFERS_NEAR_DATA: {
    offersNear: offersMock,
  },
  CITY: {
    city: 'Paris',
  },
};


describe('Component: Map', () => {
  beforeAll(() => {
    history = createMemoryHistory();

    const createFakeStore = configureStore({});
    store = createFakeStore(fakeDataStore);
  });

  it('should render correctly', () => {
    const CITY =Cities[fakeDataStore.CITY.city.toUpperCase()];

    const getCitySortOffers = toSortOffers(offersMock, CITY, 'Popular');

    const useMapFake = jest.spyOn(useMap, 'default');
    useMapFake.mockReturnValue({
      flyTo: () => [],
    },
    );

    const { container } = render(
      <Provider store={store}>
        <Router history={history}>
          <Map isActiveCard={offersMock[0]} offers={getCitySortOffers} city={CITY.name} />
        </Router>
      </Provider>,
    );
    const mapRef = container.querySelector('.map');
    expect(mapRef).toBeInTheDocument();
    expect(useMapFake).toBeCalled();
    expect(useMapFake).toBeCalledWith( {current: mapRef}, CITY);
  });
});
