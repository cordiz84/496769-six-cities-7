import React, {useEffect, useRef} from 'react';
import leaflet from 'leaflet';
import 'leaflet/dist/leaflet.css';
import useMap from '../../hooks/use-map';
import {Cities, MapMarker} from '../../const';
import CardProp from '../card/card.prop';
import PropTypes from 'prop-types';

function Map({isActiveCard, offers, city}) {
  const CITY =Cities[city.toUpperCase()];
  const mapRef = useRef(null);
  const map = useMap(mapRef, CITY);
  const defaultIcon = leaflet.icon(MapMarker.DEFAULT);
  const activeIcon = leaflet.icon(MapMarker.ACTIVE);

  useEffect(() => {
    if (map) {
      const markers = offers.map(({location, id}) => leaflet
        .marker({
          lat: location.latitude,
          lng: location.longitude,
        }, {
          icon: (isActiveCard && id === isActiveCard.id)
            ? activeIcon
            : defaultIcon,
        }));
      markers.forEach((marker) => marker.addTo(map));
      map.flyTo([CITY.location.latitude, CITY.location.longitude], CITY.location.zoom, {
        animate: true,
        duration: 2,
      });
      return () => {
        markers.forEach((marker) => marker.removeFrom(map));
      };
    }
  }, [map, offers, isActiveCard, activeIcon, defaultIcon, CITY]);

  return (
    <div className={'map'} style={{height: '100%'}} ref={mapRef}/>
  );
}

Map.propTypes = {
  isActiveCard: CardProp,
  offers: PropTypes.arrayOf(CardProp),
  city: PropTypes.string.isRequired,
};

export default Map;
