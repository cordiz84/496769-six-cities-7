import React from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {AppRoute, AuthorizationStatus} from '../../const';
import {signOut} from '../../store/api-actions';
import {getAuthorizationStatus, getUser} from '../../store/user/selectors';

function Header() {
  const authorizationStatus = useSelector(getAuthorizationStatus);
  const user = useSelector(getUser);
  const isAuthorization = AuthorizationStatus.AUTH === authorizationStatus;
  const {avatarUrl, email} = user  || {};

  const dispatch = useDispatch();

  const handlerLogout = function () {
    dispatch(signOut());
  };

  return (
    <header className="header" data-testid="header">
      <div className="container">
        <div className="header__wrapper">
          <div className="header__left">
            <Link data-testid="header_link" className="header__logo-link header__logo-link--active"  to="/">
              <img className="header__logo" src="img/logo.svg" alt="6_cities_logo" width="81" height="41"/>
            </Link>
          </div>
          <nav className="header__nav">
            <ul className="header__nav-list">
              {isAuthorization &&
              <li className="header__nav-item user">
                <Link data-testid="favorites_link" className="header__nav-link header__nav-link--profile" to={AppRoute.FAVORITES}>
                  <div className="header__avatar-wrapper user__avatar-wrapper" style={avatarUrl && {backgroundImage:`url(${avatarUrl})`, borderRadius: '50%'}}>
                  </div>
                  <span className="header__user-name user__name">{email}</span>
                </Link>
              </li>}
              <li className="header__nav-item">
                {isAuthorization ?
                  <div data-testid="sign_out_link" className="header__nav-link" onClick={handlerLogout}>
                    <span className="header__login">Sign out</span>
                  </div>
                  :
                  <Link data-testid="sign_in_link" className="header__nav-link" to={AppRoute.SIGN_IN}>
                    <span className="header__signout">Sign in</span>
                  </Link>}
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
}

export default Header;
