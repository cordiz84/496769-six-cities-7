import React from 'react';
import {render, screen} from '@testing-library/react';
import {Route, Router, Switch} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import userEvent from '@testing-library/user-event';
import Header from './header';
import configureStore from 'redux-mock-store';
import {AppRoute, AuthorizationStatus} from '../../const';
import {Provider} from 'react-redux';
import * as Redux from 'react-redux';

let history = null;
let store = null;
let fakeApp = null;

const fakeDataStore = {
  USER: {
    authorizationStatus: AuthorizationStatus.AUTH,
    user: {
      id:1,
      email:'test1@ya.ru',
      name:'test1',
      avatarUrl:'https://7.react.pages.academy/static/avatar/6.jpg',
      isPro:false,
      token:'',
    },
  },
};

describe('Component: Header', () => {

  beforeAll(() => {
    history = createMemoryHistory();

    const createFakeStore = configureStore({});
    store = createFakeStore(fakeDataStore);
  });

  it('should render correctly', () => {
    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Header />
        </Router>
      </Provider>
    );
    render(fakeApp);
    expect(screen.getByRole('img')).toHaveAttribute('src', 'img/logo.svg');
    expect(screen.getByText('Sign out')).toBeInTheDocument();
  });

  it('should redirect to root url when user clicked to link', () => {
    history.push('/fake');
    render(
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path="/" exact>
              <h1>This is main page</h1>
            </Route>
            <Route>
              <Header />
            </Route>
          </Switch>
        </Router>
      </Provider>,
    );

    expect(screen.queryByText(/This is main page/i)).not.toBeInTheDocument();
    userEvent.click(screen.getByTestId('header_link'));
    expect(screen.queryByText(/This is main page/i)).toBeInTheDocument();
  });

  it('should redirect to favorites url when user clicked to link', () => {
    render(
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path={AppRoute.FAVORITES} exact>
              <h1>This is favorites page</h1>
            </Route>
            <Route>
              <Header />
            </Route>
          </Switch>
        </Router>
      </Provider>,
    );
    userEvent.click(screen.getByTestId('favorites_link'));
    expect(screen.queryByText(/This is favorites page/i)).toBeInTheDocument();
  });

  it('should redirect to sign in url when user clicked to link', () => {
    fakeDataStore.USER.authorizationStatus = AuthorizationStatus.NO_AUTH;
    render(
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path={AppRoute.SIGN_IN} exact>
              <h1>This is Sign In page</h1>
            </Route>
            <Route>
              <Header />
            </Route>
          </Switch>
        </Router>
      </Provider>,
    );

    userEvent.click(screen.getByTestId('sign_in_link'));
    expect(screen.queryByText(/This is Sign In page/i)).toBeInTheDocument();
  });

  it('should redirect to sign out url when user clicked to link', () => {
    fakeDataStore.USER.authorizationStatus = AuthorizationStatus.AUTH;
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);
    render(
      <Provider store={store}>
        <Router history={history}>
          <Header />
        </Router>
      </Provider>,
    );
    expect(screen.getByText('Sign out')).toBeInTheDocument();
    userEvent.click(screen.getByTestId('sign_out_link'));
    expect(dispatch).toBeCalled();
  });

});
