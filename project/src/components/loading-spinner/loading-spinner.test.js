import React from 'react';
import {render} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import LoadingSpinner from './loading-spinner';

describe('Component: LoadingSpinner', () => {
  it('should render correctly', () => {
    const history = createMemoryHistory();
    const {getByTestId} = render(
      <Router history={history}>
        <LoadingSpinner />
      </Router>,
    );

    expect(getByTestId('spinner')).toBeInTheDocument();
  });
});
