import React from 'react';

function LoadingSpinner() {
  return (
    <p data-testid="spinner" style={{
      position: 'fixed',
      left: '0px',
      top: '0px',
      width: '100%',
      height: ' 100%',
      background: 'url("https://mir-s3-cdn-cf.behance.net/project_modules/disp/04de2e31234507.564a1d23645bf.gif") 50% 50% no-repeat',
      backgroundSize: '10%',
    }}
    >
    </p>
  );
}

export default LoadingSpinner;
