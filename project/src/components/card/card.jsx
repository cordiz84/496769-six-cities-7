import React, {memo} from 'react';
import CardProp from './card.prop';
import {ratingToPercentage} from '../../helpers/helpers';
import {Link, useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import {AppRoute, AuthorizationStatus, TypeCard} from '../../const';
import {addIsFavorites} from '../../store/api-actions';
import {useDispatch, useSelector} from 'react-redux';
import {getAuthorizationStatus} from '../../store/user/selectors';

const propMap = {
  [TypeCard.NEAR]: {
    articleClass: 'near-places__card',
    divImageWrapperClass: 'near-places',
  },
  [TypeCard.CITIES]: {
    articleClass: 'cities__place-card',
    divImageWrapperClass: 'cities',
  },
  [TypeCard.FAVORITES]: {
    articleClass: 'favorites__card',
    divImageWrapperClass: 'favorites',
    divInfoClass: 'favorites__image-wrapper',
  },
};

function Card({offer, hoverHandler, typeCard}) {
  const {id, previewImage, title, price, rating, type, isPremium, isFavorite} = offer;
  const dispatch = useDispatch();
  const authorizationStatus= useSelector(getAuthorizationStatus);
  const history = useHistory();

  const handleFavorite = function (event) {
    event.preventDefault();
    if (authorizationStatus === AuthorizationStatus.AUTH) {
      dispatch(addIsFavorites(id, !isFavorite));
    } else {
      history.push(AppRoute.SIGN_IN);
    }
  };

  return (
    <article className={`${propMap[typeCard].articleClass} place-card`}
      onMouseEnter={() => typeCard === 'city' && hoverHandler(offer)} onMouseLeave={() => typeCard === 'city' && hoverHandler(null)}
    >
      {isPremium && <div data-testid='premium' className="place-card__mark"><span>Premium</span></div>}
      <div className={`${propMap[typeCard].divImageWrapperClass} place-card__image-wrapper`}>
        <Link data-testid="preview_image" to={`/hotels/${id}`}>
          <img className="place-card__image" src={previewImage} width="260" height="200"
            alt="Place_image"
          />
        </Link>
      </div>
      <div className={`${typeCard && propMap[typeCard].divInfoClass} place-card__info`}>
        <div className="place-card__price-wrapper">
          <div className="place-card__price">
            <b className="place-card__price-value">&euro;{price}</b>
            <span className="place-card__price-text">&#47;&nbsp;night</span>
          </div>
          <button data-testid="to-bookmark" className={`${isFavorite ? 'place-card__bookmark-button--active' : ''} place-card__bookmark-button button`}
            type="button"
            onClick={handleFavorite}
          >
            <svg className="place-card__bookmark-icon" width="18" height="19">
              <use xlinkHref="#icon-bookmark"/>
            </svg>
            <span className="visually-hidden">To bookmarks</span>
          </button>
        </div>
        <div className="place-card__rating rating">
          <div className="place-card__stars rating__stars">
            <span data-testid="rating" style={{width: `${ratingToPercentage(rating)}%`}}/>
            <span className="visually-hidden">Rating</span>
          </div>
        </div>
        <h2 className="place-card__name">
          <Link data-testid="link_title"  to={`/hotels/${id}`}>{title}</Link>
        </h2>
        <p className="place-card__type">{type}</p>
      </div>
    </article>
  );
}

Card.propTypes = {
  offer: CardProp.isRequired,
  hoverHandler: PropTypes.func,
  typeCard: PropTypes.oneOf(Object.values(TypeCard)),
};

export default memo(Card);
