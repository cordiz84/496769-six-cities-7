import React from 'react';
import {fireEvent, render, screen} from '@testing-library/react';
import {Route, Router, Switch} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {AppRoute, AuthorizationStatus, TypeCard} from '../../const';
import * as Redux from 'react-redux';

import Card from './card';
import {offersMock} from '../app/app.mock';
import {ratingToPercentage} from '../../helpers/helpers';
import userEvent from '@testing-library/user-event';

let history = null;
let store = null;
let fakeApp = null;
const mockStoreData = {
  USER: {
    authorizationStatus: AuthorizationStatus.AUTH,
  },
};

const propMap = {
  [TypeCard.NEAR]: {
    articleClass: 'near-places__card',
    divImageWrapperClass: 'near-places',
  },
  [TypeCard.CITIES]: {
    articleClass: 'cities__place-card',
    divImageWrapperClass: 'cities',
  },
  [TypeCard.FAVORITES]: {
    articleClass: 'favorites__card',
    divImageWrapperClass: 'favorites',
    divInfoClass: 'favorites__image-wrapper',
  },
};

describe('Component: Card', () => {

  const mockCardData = {
    offer: offersMock[0],
    typeCard: 'city',
    hoverHandler: () => {},
  };

  beforeAll(() => {
    history = createMemoryHistory();
    const createFakeStore = configureStore({});

    store = createFakeStore(mockStoreData);
    const {offer, typeCard, hoverHandler} = mockCardData;
    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Card
            offer={offer}
            hoverHandler={hoverHandler}
            typeCard={typeCard}
          />
        </Router>
      </Provider>
    );
  });

  it('should render correctly', () => {
    const {offer, typeCard, hoverHandler} = mockCardData;
    render(fakeApp);
    expect(screen.getByRole('article')).toHaveClass(`${propMap[typeCard].articleClass} place-card`);
    expect(screen.getByText(offer.title)).toBeInTheDocument();
    expect(screen.getByText(offer.price, { exact: false })).toBeInTheDocument();
    expect(screen.getByText(offer.type,{ exact: false })).toBeInTheDocument();
    expect(hoverHandler).toBeInstanceOf(Function);
    expect(screen.getByRole('img')).toHaveAttribute('src', offer.previewImage);
    expect(screen.getByTestId('rating')).toHaveStyle(`width: ${ratingToPercentage(offer.rating)}%`);
  });

  it('should render isFavorite & isPremium', () => {
    const {offer} = mockCardData;
    offer.isFavorite = true;
    offer.isPremium = true;
    const dispatch = jest.fn();
    const useDispatch = jest.spyOn(Redux, 'useDispatch');
    useDispatch.mockReturnValue(dispatch);

    render(fakeApp);
    expect(screen.getByRole('button')).toHaveClass('place-card__bookmark-button--active');
    expect(screen.getByTestId('premium')).toHaveClass('place-card__mark');
    userEvent.click(screen.getByTestId('to-bookmark'));
    expect(dispatch).toBeCalled();
  });

  it('should render typeCard favorite', () => {
    const {offer, hoverHandler} = mockCardData;

    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Card
            offer={offer}
            hoverHandler={hoverHandler}
            typeCard={'favorites'}
          />
        </Router>
      </Provider>
    );
    render(fakeApp);

    expect(screen.getByRole('article')).toHaveClass('favorites__card place-card');
  });

  it('should user hover Card', () => {
    const {offer, typeCard} = mockCardData;
    const hoverHandler = jest.fn();
    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Card
            offer={offer}
            hoverHandler={hoverHandler}
            typeCard={typeCard}
          />
        </Router>
      </Provider>
    );
    render(fakeApp);
    userEvent.hover(screen.getByRole('article'));
    expect(hoverHandler).toBeCalled();
    expect(hoverHandler).toBeCalledWith(offer);
    userEvent.unhover(screen.getByRole('article'));
    expect(hoverHandler).toBeCalled();
    expect(hoverHandler).toBeCalledWith(null);
  });

  it('should click link to Room Screen', () => {
    const {offer, typeCard} = mockCardData;
    const hoverHandler = jest.fn();
    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path={`${AppRoute.ROOM}/${offer.id}`}>
              <p>This is mock Room Screen</p>
            </Route>
            <Route>
              <Card
                offer={offer}
                hoverHandler={hoverHandler}
                typeCard={typeCard}
              />
            </Route>
          </Switch>
        </Router>
      </Provider>
    );
    render(fakeApp);

    userEvent.click(screen.getByTestId('preview_image'));
    expect(screen.getByText(/This is mock Room Screen/i)).toBeInTheDocument();
    history.push(AppRoute.MAIN);
    userEvent.click(screen.getByTestId('link_title'));
    expect(screen.getByText(/This is mock Room Screen/i)).toBeInTheDocument();
  });

  it('should click is favorites no auth', () => {
    const {offer, typeCard} = mockCardData;
    const hoverHandler = jest.fn();

    mockStoreData.USER.authorizationStatus=AuthorizationStatus.NO_AUTH;
    history.push('/fake');
    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <Switch>
            <Route path={AppRoute.SIGN_IN}>
              <p>This is mock Login</p>
            </Route>
            <Route>
              <Card
                offer={offer}
                hoverHandler={hoverHandler}
                typeCard={typeCard}
              />
            </Route>
          </Switch>
        </Router>
      </Provider>
    );
    render(fakeApp);

    fireEvent.click(screen.getByTestId('to-bookmark'));
  });
});
