import React from 'react';
import PropTypes from 'prop-types';
import {Route, Redirect} from 'react-router-dom';

function PrivateRoute({render, path, exact, redirect, authStatus}) {
  return (
    <Route
      path={path}
      exact={exact}
      render={(routeProps) => (
        authStatus
          ? render(routeProps)
          : <Redirect to={redirect} />
      )}
    />
  );
}

PrivateRoute.propTypes = {
  exact: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
  render: PropTypes.func.isRequired,
  redirect: PropTypes.string.isRequired,
  authStatus: PropTypes.bool.isRequired,
};

export default PrivateRoute;
