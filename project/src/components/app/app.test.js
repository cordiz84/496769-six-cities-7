import React from 'react';
import {render, screen} from '@testing-library/react';
import {Router} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {AuthorizationStatus, AppRoute, APIRoute} from '../../const';
import App from './app';
import {reviews} from '../../store/action-test-mocks';
import {offersMock} from './app.mock';
import {convertToCamel} from '../../helpers/helpers';

let history = null;
let store = null;
let fakeApp = null;

jest.mock('../pages/room/room', () => {
  const roomScreen = () => <>This is mock Room Screen</>;
  return {
    __esModule: true,
    default: roomScreen,
  };
});

jest.mock('../pages/favorites/favorites', () => {
  const favoritesScreen = () => <>This is mock Favorites Screen</>;
  return {
    __esModule: true,
    default: favoritesScreen,
  };
});

const mockDataStore ={
  USER: {
    authorizationStatus: AuthorizationStatus.AUTH,
    user: {
      id:1,
      email:'test1@ya.ru',
      name:'test1',
      avatarUrl:'https://7.react.pages.academy/static/avatar/6.jpg',
      isPro:false,
      token:'',
    },
  },
  OFFERS_DATA: {
    offers: offersMock,
    isDataLoaded:true,
    isDataLoadedById: true,
    isFavoritesLoad: true,
    city: 'Paris',
    sortOption: 'Popular',
    offer: offersMock[0],
  },
  OFFERS_NEAR_DATA: {
    offersNear: offersMock,
  },
  REVIEWS_DATA: {
    reviews: convertToCamel(reviews),
  },
  CITY: {
    city: 'Paris',
  },
  SORT_OPTION: {
    sortOption: 'Popular',
  },
  ERROR: {
    errors: null,
  },
};

describe('Application Routing', () => {
  beforeAll(() => {
    history = createMemoryHistory();

    const createFakeStore = configureStore({});
    store = createFakeStore(mockDataStore);

    fakeApp = (
      <Provider store={store}>
        <Router history={history}>
          <App />
        </Router>
      </Provider>
    );
  });

  it('should render "Main" when user navigate to "/"', () => {
    history.push(AppRoute.MAIN);
    render(fakeApp);

    expect(screen.getByText(/places to stay in/i)).toBeInTheDocument();
  });

  it('should render "SignIn" when user navigate to "/login"', () => {
    mockDataStore.USER.authorizationStatus = AuthorizationStatus.NO_AUTH;
    history.push(AppRoute.SIGN_IN);
    render(fakeApp);
    expect(screen.getByPlaceholderText(/Password/i)).toBeInTheDocument();
    expect(screen.getByPlaceholderText(/Email/i)).toBeInTheDocument();
  });

  it('should render "Favorites" when user navigate to "/favorites"', () => {
    mockDataStore.USER.authorizationStatus = AuthorizationStatus.AUTH;
    history.push(AppRoute.FAVORITES);
    render(fakeApp);
    expect(screen.getByText(/This is mock Favorites Screen/i)).toBeInTheDocument();
  });

  it('should render "Room" when user navigate to "/hotels/2"', () => {
    history.push(`${AppRoute.ROOM}/2`);
    render(fakeApp);
    expect(screen.getByText(/This is mock Room Screen/i)).toBeInTheDocument();
  });

  it('should render "NotFoundLoad" when user navigate to not found load', () => {
    history.push(APIRoute.FILED_LOAD_DATA);
    render(fakeApp);

    expect(screen.getByText('Error Page Load')).toBeInTheDocument();
    expect(screen.getByText('Go back to the main page')).toBeInTheDocument();
  });

  it('should render NotFound when user navigate to non-existent route', () => {
    history.push('/not-found');
    render(fakeApp);

    expect(screen.getByText('404. Page not found')).toBeInTheDocument();
    expect(screen.getByText('Вернуться на главную')).toBeInTheDocument();
  });
});
