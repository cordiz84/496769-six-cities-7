import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Main from '../pages/main/main';
import SignIn from '../pages/signin/signin';
import Favorites from '../pages/favorites/favorites';
import Room from '../pages/room/room';
import NotFound from '../pages/not-found/not-found';
import {APIRoute, AppRoute, AuthorizationStatus} from '../../const';
import {useSelector} from 'react-redux';
import LoadingSpinner from '../loading-spinner/loading-spinner';
import {isCheckedAuth} from '../../helpers/helpers';
import PrivateRoute from '../private-route/private-route';
import {getIsDataLoaded, getOffers} from '../../store/offers/selectors';
import {getAuthorizationStatus} from '../../store/user/selectors';
import MainEmpty from '../pages/main/main-empty';
import FailedLoadData from '../pages/failed-load-data/failed-load-data';

function App() {
  const offers= useSelector(getOffers);
  const isDataLoaded= useSelector(getIsDataLoaded);
  const authorizationStatus= useSelector(getAuthorizationStatus);
  const offersCount = offers.length;
  const isAuthorization = authorizationStatus === AuthorizationStatus.AUTH;

  if (!isCheckedAuth(authorizationStatus) || !isDataLoaded) {
    return (
      <LoadingSpinner />
    );
  }

  return (
    <Switch>
      <Route path={AppRoute.MAIN} exact >
        {offersCount !== 0 ? <Main offers={offers}/> : <MainEmpty />}
      </Route>
      <PrivateRoute
        exact
        path={AppRoute.SIGN_IN}
        render={() => <SignIn/>}
        redirect={AppRoute.MAIN}
        authStatus={!isAuthorization}
      />
      <PrivateRoute
        exact
        path={AppRoute.FAVORITES}
        render={() => <Favorites/>}
        redirect={AppRoute.SIGN_IN}
        authStatus={isAuthorization}
      />
      <Route exact path={`${AppRoute.ROOM}/:id`} render={({match}) => (
        <Room offerId={match.params.id} />
      )}
      />
      <Route  path={APIRoute.FILED_LOAD_DATA} exact >
        <FailedLoadData />
      </Route>
      <Route>
        <NotFound />
      </Route>
    </Switch>
  );
}

export default App;
