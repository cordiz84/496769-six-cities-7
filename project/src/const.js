const AppRoute = {
  MAIN: '/',
  SIGN_IN: '/login',
  FAVORITES: '/favorites',
  ROOM: '/hotels',
};

export const APIRoute = {
  OFFERS: '/hotels',
  LOGIN: '/login',
  LOGOUT: '/logout',
  REVIEWS: '/comments',
  FAVORITES: '/favorite',
  FILED_LOAD_DATA: '/failed_load_data',
};

export const AuthorizationStatus = {
  AUTH: 'AUTH',
  NO_AUTH: 'NO_AUTH',
  UNKNOWN: 'UNKNOWN',
};

const MapMarker = {
  DEFAULT: {
    iconUrl: 'img/pin.svg',
    iconSize: [30, 30],
    iconAnchor: [15, 30],
  },
  ACTIVE: {
    iconUrl: 'img/pin-active.svg',
    iconSize: [30, 30],
    iconAnchor: [15, 30],
  },
};

const Cities = {
  PARIS:{
    location: {
      latitude: 48.85661,
      longitude: 2.351499,
      zoom: 13,
    },
    name: 'Paris',
  },
  COLOGNE:{
    location: {
      latitude: 50.938361,
      longitude: 6.959974,
      zoom: 13,
    },
    name: 'Cologne',
  },
  BRUSSELS:{
    location: {
      latitude: 50.846557,
      longitude: 4.351697,
      zoom: 13,
    },
    name: 'Brussels',
  },
  AMSTERDAM:{
    location: {
      latitude: 52.37454,
      longitude: 4.897976,
      zoom: 13,
    },
    'name': 'Amsterdam',
  },
  HAMBURG: {
    location: {
      latitude:  53.550341,
      longitude: 10.000654,
      zoom: 13,
    },
    name: 'Hamburg',
  },
  DUSSELDORF: {
    location: {
      latitude: 51.225402,
      longitude: 6.776314,
      zoom: 13,
    },
    name: 'Dusseldorf',
  },
};
const SortOptions = {
  POPULAR: 'Popular',
  LOW_TO_HIGH: 'lowToHigh',
  HIGH_TO_LOW: 'highToLow',
  RATED_FIRST: 'ratedFirst',
};

const TypeCard = {
  FAVORITES: 'favorites',
  CITIES: 'city',
  NEAR: 'near',
};

export{AppRoute, MapMarker, Cities, SortOptions, TypeCard};
